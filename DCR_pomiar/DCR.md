Parametr DCR cewki indukcyjnej
Termin DCR oznacza rezystancję DC. Wartość ta reprezentuje opór, jaki może zaoferować 
cewka indukcyjna, gdy przepuszczany jest przez nią sygnał prądu stałego o częstotliwości 
0 Hz.
Rezystancja cewki jest proporcjonalna do długości drutu użytego do utworzenia cewki, a 
długość cewki jest również proporcjonalna do wartości indukcyjności cewki.
𝑅 = 𝜌 ∗ 𝑙𝑎
- • 𝜌 oporność
- • 𝑙 długość 
- • 𝑎 powierzchnia przekroju 
Duża wartość indukcyjności wymaga większej liczby uzwojeń niż cewki o niskiej wartości, co 
zwiększa długość drutu. DCR cewek zwykle waha się od kilka 𝑚Ω do kilka kΩ.
Pomiar wartości DCR cewki indukcyjnej jest przy użycie ścieżki wykrywania Kelvina przez 
przewody i doprowadzenie prądu do cewki indukcyjnej. Prawo Ohma, 𝑉 = 𝐼 𝑥 𝑅
Schemat:
![image](/uploads/9b4fc9a0af47ed93b414e569456b81d8/image.png)

Zakres:
* Najmniejsza wykrywalna indukcyjność wynosi 0,01 mH 
* Największa wykrywalna indukcyjność wynosi 1 H
* Metoda ta nie może być stosowana w technice próbkowania
* DCR nie może być większy niż 2,1 kΩ

Normalny pomiar indukcyjności opiera się na pomiarze stałej czasowej obecny wzrost. Granica wykrywalności wynosi około 0,01 mH, jeśli rezystancja cewki jest mniejsza niż 24 Ω. Dla większych wartościach rezystancji rozdzielczość wynosi tylko 0,1 mH. Jeśli rezystancja przekracza 2,1 kΩ, ta technika nigdy nie może być używany do wykrywania cewek. Wyniki pomiaru tego normalnego pomiaru są pokazane w drugi linii (rezystancja i indukcyjność). 

Metodę pomiaru częstotliwości rezonansowej można również wykorzystać do wyznaczenia wartości indukcyjności, jeżeli odpowiednio duży kondensator o znanej pojemności zostanie podłączony równolegle do małej indukcyjności (<2mH). Z równolegle podłączonym kondensatorem normalny pomiar indukcyjności nie może już dobrze funkcjonować. 

Metoda pomiaru opiera się na narastaniu prądu według wzoru . I_l=I_max*(1-exp⁡〖(-t)/τ〗) po załączeniu prądu. Stała czasowa τ =L/R jest proporcjonalny do indukcyjności L, ale odwrotnie proporcjonalny do rezystora R. Prąd można mierzyć tylko pośrednio ze spadkiem potencjału rezystora. Niestety stała czasowa zostanie dodatkowo zmniejszona przez stosunkowo dużą rezystancję 680 Ω, przez co pomiar małych wartości indukcyjności jest dodatkowo utrudniony przy zegarze 8 MHz. Aby uzyskać stałą czasową, napięcie na rezystorze 680 Ω będzie monitorowane przez komparator analogowy. Jeżeli spadek napięcia na rezystorze 680 Ω będzie większy niż napięcie odniesienia wewnętrznego, zdarzenie to zostanie zgłoszone do licznika 16-bitowego, który zostanie uruchomiony w momencie załączenia prądu. Licznik zapisze stan tego zdarzenia. Jeśli licznik zostanie przekroczony, zostanie to zliczone przez program. Po zdarzeniu licznik zostanie zatrzymany przez program i czas całkowity zostanie zbudowany z zapisanego stanu licznika i licznika przepełnień. Dodatnia strona cewki zostanie przełączona z VCC na GND i utrzyma się w tym etapie, dopóki monitorowanie napięć obu pinów nie wykaże, że nie wykryto prądu. Na rysunku przedstawiono uproszczony schemat sytuacji pomiarowej.

Mając napięcie zasilania VCC i sumę wszystkich oporników w obwodzie elektrycznym, można obliczyć maksymalny prąd I_max iz tego procent napięcia odniesienia do maksymalnego napięcia na oporniku 680 Ω U_max  = I_max* (680 + 19). Za pomocą wzoru: L = -(t*R_ges)/log⁡(1-U_ref/U_max )    można obliczyć indukcyjność. Logarytm zostanie pobrany z wbudowanej tabeli. Dla tego typu pomiaru przyjmuje się rozdzielczość indukcyjności równą 0,1 mH. Aby zmierzyć również niższe wartości indukcyjności, rezystor 680 Ω zostanie pominięty w pętli prądowej, jeśli wartość rezystancji cewki indukcyjnej jest mniejsza niż 24 Ω. Do pomiaru prądu wykorzystana zostanie tylko rezystancja wyjściowa portu (19 Ω). W tym szczególnym przypadku prąd szczytowy będzie większy niż wartość, na którą pozwala specyfikacja ATmega. Ponieważ będzie to prawdą tylko przez zauważalnie krótki czas, nie spodziewam się uszkodzenia portów ATmegi. Do tego typu pomiaru wybiera się rozdzielczość indukcyjności 0,01 mH.

Aby uniknąć dłuższego czasu z nadmiernym prądem, dodatkowy pomiar z opóźnionym startem licznika będzie zawsze wykonywany z rezystorem 680 Ω.

Aby uzyskać lepsze dopasowanie wyników pomiarów, jeśli pomiar jest wykonywany bez rezystora 680 Ω, od stanu licznika odejmuje się przesunięcie zera równe 6. W przeciwnym razie odejmowane jest przesunięcie zera równe 7 lub 8. Przy dużych wartościach indukcyjności pojemność pasożytnicza może spowodować szybki wzrost prądu, dzięki czemu komparator zareaguje natychmiast. Aby mimo wszystko uzyskać wartość indukcyjności, pomiar zostanie powtórzony z opóźnionym startem licznika. Dzięki tej metodzie wzrost napięcia spowodowany wzrostem prądu cewki indukcyjnej zostanie wykryty przez komparator analogowy zamiast aktualnego szczytu pojemności pasożytniczej. Pomiary są zawsze wykonywane w obu kierunkach prądu. Program wybierze jako wyświetlany wynik wyższy wynik pomiaru dla tego samego kierunku prądu, ale wynik niższy dla innego kierunku prądu.

Najmniejsza wykrywalna indukcyjność wynosi 0,01 mH przy normalnej technice pomiarowej. Która wykorzystuje prędkość wzrostu prądu do pomiaru indukcyjności. Metoda ta nie może być stosowana w technice próbkowania, ponieważ obwód pomiarowy nie wykorzystuje dodatkowych rezystorów z cewką. Prąd wzrośnie do niedozwolonych wartości bardzo szybko. Uszkodzeniu ATmegi możemy zapobiec tylko poprzez niezwykle szybkie przerwanie przepływu prądu. W przypadku metody próbkowania trudno jest uzyskać tak szybkie wyłączanie prądu, a dodatkowo ten krytyczny proces musi być wykonywany wiele razy w serii.

Przy krótkim impulsie prądu obwód ten zaczyna czasami oscylować bez dalszej stymulacji. Metodą próbkowania można zmierzyć częstotliwość tych oscylacji. Ponieważ dla tego pomiaru jedna strona cewki musi być podłączona do poziomu GND, występują dwa problemy z pomiarem. Gdy ujemne napięcie oscylacji zostanie ograniczone przez wewnętrzną diodę zabezpieczającą ATmega do około 0,6 V. Z tego powodu dodatnia część oscylacji nigdy nie osiągnie wyższego napięcia niż 0,6 V. Dodatkowo ADC ATmega może mierzyć tylko dodatnie wartości napięcia. Dlatego wszystkie ujemne części oscylacji są odczytywane jako zero.