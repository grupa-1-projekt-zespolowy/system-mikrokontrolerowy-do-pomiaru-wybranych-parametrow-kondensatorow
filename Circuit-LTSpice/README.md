# Instructions

## To run this example

1. Install LT Spice XVII
2. Open the file
3. Click Simulation -> Run
4. Probe coil voltage by clicking on the wire just above it
5. Probe coil current by clicking directly on it

![Diagram coil-circuit](./assets/circuit_diagram.png "Diagram coil-circuit")
