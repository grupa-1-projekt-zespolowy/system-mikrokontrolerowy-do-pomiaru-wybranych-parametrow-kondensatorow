# Przebieg pomiaru

1. Włączyć zasilanie układu
2. Reset układu AD5933
3. Ustawić źródło sygnału zegara na wewnętrzny zegar AD5933
4. Wykonać kalibrację AD5933
5. Ustawić startową częstotliwość pomiaru
6. Ustawić inkrement kolejnych mierzonych częstotliwości
7. Ustawić liczbę pomiarów
8. Wykonać pomiary impedancji w ustawionym zakresie
9. Wyliczyć indukcyjność uwzględniając zmierzoną impedancję i częstotliwość sygnału dla każdego pomiaru
10. Uśrednić wartość indukcyjności
11. Obliczyć błąd pomiarowy