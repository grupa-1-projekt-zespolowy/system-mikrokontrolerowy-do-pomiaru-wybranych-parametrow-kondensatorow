**System mikrokontrolerowy do pomiaru wybranych parametrów cewek**

## Założenia projektowe

Producenci cewek podają najczęściej w specyfikacji następujące parametry:
* Indukcyjność
* SRF - częstotliwość rezonansową własną
* DCR - rezystancję przy prądzie stałym
* I rms
* I sat
* K-factor

Projekt zakłada zmierzenie indukcyjności i DCR nieznanej cewki.

Zakres:
* Najmniejsza wykrywalna indukcyjność wynosi 0,01 mH 
* Największa wykrywalna indukcyjność wynosi 1 H
* Metoda ta nie może być stosowana w technice próbkowania
* DCR nie może być większy niż 2,1 kΩ

### Schemat układu
![Schemat układu][inductance]

### Pomiar indukcyjności
#### Metoda rezonansu LC


Ta metoda polega na stworzeniu obwodu LC z cewką indukcyjną i znanym kondensatorem, a następnie należy wykonać pomiar częstotliwości rezonansowej obwodu. Na podstawie częstotliwości rezonansowej i znanej wartości kondensatora można obliczyć indukcyjność za pomocą wzoru:

$(L = 1 / (4 * π^2 * f^2 * C))$

1. Przygotuj układ: Podłącz cewkę (indukcyjność do zmierzenia) wraz z znanym kondensatorem, aby utworzyć obwód rezonansowy LC.

2. Podłącz układ do Arduino: Podłącz jeden koniec obwodu rezonansowego LC do pinu wyjściowego cyfrowego Arduino. Podłącz drugi koniec obwodu rezonansowego LC do pinu masy (GND) na Arduino.

3. Skonfiguruj Arduino: Ustaw pin wyjściowy cyfrowy jako OUTPUT i ustaw go na wysoki stan (HIGH).

4. Ustaw pin cyfrowy na niski stan (LOW) i połącz cewkę i kondensator w szeregowy układ LC. 

5. Zmierz częstotliwość rezonansową: Wykorzystaj możliwości pomiaru częstotliwości Arduino do określenia rezonansowej częstotliwości obwodu LC. Użyj biblioteki "FreqCounter" dla Arduino.

Oblicz indukcyjność: Wykorzystaj zmierzoną rezonansową częstotliwość (f) i znaną wartość kondensatora (C), aby obliczyć indukcyjność (L).

Wyświetl lub wykorzystaj obliczoną indukcyjność: Możesz wyświetlić obliczoną indukcyjność na wyświetlaczu LCD lub użyć jej do dalszego przetwarzania w swoim programie Arduino.

### Pomiar DCR

Normalny pomiar indukcyjności opiera się na pomiarze stałej czasowej obecny wzrost. Granica wykrywalności wynosi około 0,01 mH, jeśli rezystancja cewki jest mniejsza niż 24 Ω. Dla większych wartościach rezystancji rozdzielczość wynosi tylko 0,1 mH. Jeśli rezystancja przekracza 2,1 kΩ, ta technika nigdy nie może być używany do wykrywania cewek. Wyniki pomiaru tego normalnego pomiaru są pokazane w drugi linii (rezystancja i indukcyjność). 

Metodę pomiaru częstotliwości rezonansowej można również wykorzystać do wyznaczenia wartości indukcyjności, jeżeli odpowiednio duży kondensator o znanej pojemności zostanie podłączony równolegle do małej indukcyjności (<2mH). Z równolegle podłączonym kondensatorem normalny pomiar indukcyjności nie może już dobrze funkcjonować. 

Metoda pomiaru opiera się na narastaniu prądu według wzoru . I_l=I_max*(1-exp⁡〖(-t)/τ〗) po załączeniu prądu. Stała czasowa τ =L/R jest proporcjonalny do indukcyjności L, ale odwrotnie proporcjonalny do rezystora R. Prąd można mierzyć tylko pośrednio ze spadkiem potencjału rezystora. Niestety stała czasowa zostanie dodatkowo zmniejszona przez stosunkowo dużą rezystancję 680 Ω, przez co pomiar małych wartości indukcyjności jest dodatkowo utrudniony przy zegarze 8 MHz. Aby uzyskać stałą czasową, napięcie na rezystorze 680 Ω będzie monitorowane przez komparator analogowy. Jeżeli spadek napięcia na rezystorze 680 Ω będzie większy niż napięcie odniesienia wewnętrznego, zdarzenie to zostanie zgłoszone do licznika 16-bitowego, który zostanie uruchomiony w momencie załączenia prądu. Licznik zapisze stan tego zdarzenia. Jeśli licznik zostanie przekroczony, zostanie to zliczone przez program. Po zdarzeniu licznik zostanie zatrzymany przez program i czas całkowity zostanie zbudowany z zapisanego stanu licznika i licznika przepełnień. Dodatnia strona cewki zostanie przełączona z VCC na GND i utrzyma się w tym etapie, dopóki monitorowanie napięć obu pinów nie wykaże, że nie wykryto prądu. Na rysunku przedstawiono uproszczony schemat sytuacji pomiarowej.

Mając napięcie zasilania VCC i sumę wszystkich oporników w obwodzie elektrycznym, można obliczyć maksymalny prąd I_max iz tego procent napięcia odniesienia do maksymalnego napięcia na oporniku 680 Ω U_max  = I_max* (680 + 19). Za pomocą wzoru: L = -(t*R_ges)/log⁡(1-U_ref/U_max )    można obliczyć indukcyjność. Logarytm zostanie pobrany z wbudowanej tabeli. Dla tego typu pomiaru przyjmuje się rozdzielczość indukcyjności równą 0,1 mH. Aby zmierzyć również niższe wartości indukcyjności, rezystor 680 Ω zostanie pominięty w pętli prądowej, jeśli wartość rezystancji cewki indukcyjnej jest mniejsza niż 24 Ω. Do pomiaru prądu wykorzystana zostanie tylko rezystancja wyjściowa portu (19 Ω). W tym szczególnym przypadku prąd szczytowy będzie większy niż wartość, na którą pozwala specyfikacja ATmega. Ponieważ będzie to prawdą tylko przez zauważalnie krótki czas, nie spodziewam się uszkodzenia portów ATmegi. Do tego typu pomiaru wybiera się rozdzielczość indukcyjności 0,01 mH.

Aby uniknąć dłuższego czasu z nadmiernym prądem, dodatkowy pomiar z opóźnionym startem licznika będzie zawsze wykonywany z rezystorem 680 Ω.

Aby uzyskać lepsze dopasowanie wyników pomiarów, jeśli pomiar jest wykonywany bez rezystora 680 Ω, od stanu licznika odejmuje się przesunięcie zera równe 6. W przeciwnym razie odejmowane jest przesunięcie zera równe 7 lub 8. Przy dużych wartościach indukcyjności pojemność pasożytnicza może spowodować szybki wzrost prądu, dzięki czemu komparator zareaguje natychmiast. Aby mimo wszystko uzyskać wartość indukcyjności, pomiar zostanie powtórzony z opóźnionym startem licznika. Dzięki tej metodzie wzrost napięcia spowodowany wzrostem prądu cewki indukcyjnej zostanie wykryty przez komparator analogowy zamiast aktualnego szczytu pojemności pasożytniczej. Pomiary są zawsze wykonywane w obu kierunkach prądu. Program wybierze jako wyświetlany wynik wyższy wynik pomiaru dla tego samego kierunku prądu, ale wynik niższy dla innego kierunku prądu.

Najmniejsza wykrywalna indukcyjność wynosi 0,01 mH przy normalnej technice pomiarowej. Która wykorzystuje prędkość wzrostu prądu do pomiaru indukcyjności. Metoda ta nie może być stosowana w technice próbkowania, ponieważ obwód pomiarowy nie wykorzystuje dodatkowych rezystorów z cewką. Prąd wzrośnie do niedozwolonych wartości bardzo szybko. Uszkodzeniu ATmegi możemy zapobiec tylko poprzez niezwykle szybkie przerwanie przepływu prądu. W przypadku metody próbkowania trudno jest uzyskać tak szybkie wyłączanie prądu, a dodatkowo ten krytyczny proces musi być wykonywany wiele razy w serii.

Przy krótkim impulsie prądu obwód ten zaczyna czasami oscylować bez dalszej stymulacji. Metodą próbkowania można zmierzyć częstotliwość tych oscylacji. Ponieważ dla tego pomiaru jedna strona cewki musi być podłączona do poziomu GND, występują dwa problemy z pomiarem. Gdy ujemne napięcie oscylacji zostanie ograniczone przez wewnętrzną diodę zabezpieczającą ATmega do około 0,6 V. Z tego powodu dodatnia część oscylacji nigdy nie osiągnie wyższego napięcia niż 0,6 V. Dodatkowo ADC ATmega może mierzyć tylko dodatnie wartości napięcia. Dlatego wszystkie ujemne części oscylacji są odczytywane jako zero.


**Maksymalna dokładność konwertera ADC w Arduino Nano RP2040 Connect**

Pin analogowy arduino przyjmuje wartości od 0 do 1023 i jednocześnie jest możliwość ustawienia napięcia referencyjnego na 1.1 V. To daje teoretyczną rozdzielczość w przybliżeniu 1 mV. Taka rozdzielczość powinna nawet wystarczyć by zmierzyć DCR cewek z dolnego zakładanego zakresu. Jednak dokładność pomiaru będzie również zależała od użytych rezystorów i ich dokładności i trzeba będzie to też wziąć pod uwagę.

**Dobór rezystora referencyjnego R1-R6

W celu poprawy wyników rezystory R1-R6 powinien być jak najdokładniej.

Przykładowy układ pomiarowy:
[DCR][dcr]

## Aplikacja mobilna
Głównym zadaniem aplikacji mobilnej jest prezentacja wyników pomiarów.

## Błąd pomiaru
Jednostkowe wyniki pomiaru najczęściej obarczone są odstępstwem od wartości prawdziwej. Takie odstępstwa wynikają bezpośrednio z metody pomiaru. 
Będziemy musieli znaleźć sposób na estymację wartości prawdziwej z wielokrotnie powtórzonych pomiarów. Sposób ten będzie mocno powiązany ze sposobem w jaki będziemy dokonywać pomiarów.

## Podział pracy

[![](https://mermaid.ink/img/pako:eNqVlc1u4zYQx19loFML2IFlyR_RbVtj0aIbNGgKLHbhC0UyDi2JFGiqqhTkEjToMyz2MXpb9Nb4vTqUKDrOxwYr24Atmb_5z8x_yOuAKsaDJNgQacxaAl5GmJzDRbMzvIBCZFplShqtcq5V3QBTUKpCEF1B3aSayIZeQUk0KbjR__1TA-U1z3oUI4a_VbogBmC1Gp-djT_gtZb90x2nRigJ58Toxq0479DwDsZwQa-4XSkYt3GHK2FK8hFQLcwISDiC6WQajSfReLoYwZI9wXzkKPL-bxRbEMySA895waVpoMr2t4RVjuiAU_xcGq47dBg_5a1aQXDd9v6ODghXEK8yIZjYHweRsWdOAVDwM9RzrTZYQltdle72t9VGALxZzU6jCJKBEx04Nu2XKXWDLat6ib02EEXJGZF0KyBxuJnHRc-nOuBUmgvaupSFZFVGm61U-89Y1haqut3k95-YFBzfBaT7W_xVDS4Zos19tNmDaMdO-EntdqQ40rH68bfXvNAHSI-sYOv8yAw96ZvskHo3pM-4oQd-sx9S30aL78xwXIX3aksFp1fPBPtKgzVvcWRdh8FF7APGQ2XicYghwzn7Cvqo2Qfo_afXW-2clXpj2cjh0wQv2qZQ7t6bMhcZoVuCEn7NNN9_zi3b9Wf_1yEXNwUsfJTN0JWHpPcN6rxjQNw9AfiSKOmSb3dQ7f9tTKZqic-Qz3yXWfgC7xdVVNL9wjLoFuFtXwM3ncx3lb0k6q2QJJcEaq532y6w3xjs6q4tx4X6GSVjU-xylVbOXXaP9QbDvbeGLWeyKR7IqnrMo6lxk0hDP4m2PcM28vpgfCfLEyh1yzOy_4LVEN87JPUFtOyFA_5QMVWjcPSP4bSVfKMOM9YXjfqiWYSX8jvfYdjLrlzdKp8Y8Zsh9SazkJlb-U5tbE_L3stVpxY9eucMbWG2dGjatgE8dVJSiEfuBRo_zAZFBaOg4HiICYYH5bUNtA4MVpavgwS_MqKzdbCWN_g_Uhl10UgaJEZXfBRUpT0CV4JYOUFySfId3i2JDJLr4M8gGceny5PlZLpczueL-TIMo3gUNEEyPZ2chPE8jpfRIgpnYXQzClqlEBGeTBeniyiehDN8vljiqWd5H7uHfVDOhFH6rD_Yu_P95n_O2aRQ?type=png)](https://mermaid.live/edit#pako:eNqVlc1u4zYQx19loFML2IFlyR_RbVtj0aIbNGgKLHbhC0UyDi2JFGiqqhTkEjToMyz2MXpb9Nb4vTqUKDrOxwYr24Atmb_5z8x_yOuAKsaDJNgQacxaAl5GmJzDRbMzvIBCZFplShqtcq5V3QBTUKpCEF1B3aSayIZeQUk0KbjR__1TA-U1z3oUI4a_VbogBmC1Gp-djT_gtZb90x2nRigJ58Toxq0479DwDsZwQa-4XSkYt3GHK2FK8hFQLcwISDiC6WQajSfReLoYwZI9wXzkKPL-bxRbEMySA895waVpoMr2t4RVjuiAU_xcGq47dBg_5a1aQXDd9v6ODghXEK8yIZjYHweRsWdOAVDwM9RzrTZYQltdle72t9VGALxZzU6jCJKBEx04Nu2XKXWDLat6ib02EEXJGZF0KyBxuJnHRc-nOuBUmgvaupSFZFVGm61U-89Y1haqut3k95-YFBzfBaT7W_xVDS4Zos19tNmDaMdO-EntdqQ40rH68bfXvNAHSI-sYOv8yAw96ZvskHo3pM-4oQd-sx9S30aL78xwXIX3aksFp1fPBPtKgzVvcWRdh8FF7APGQ2XicYghwzn7Cvqo2Qfo_afXW-2clXpj2cjh0wQv2qZQ7t6bMhcZoVuCEn7NNN9_zi3b9Wf_1yEXNwUsfJTN0JWHpPcN6rxjQNw9AfiSKOmSb3dQ7f9tTKZqic-Qz3yXWfgC7xdVVNL9wjLoFuFtXwM3ncx3lb0k6q2QJJcEaq532y6w3xjs6q4tx4X6GSVjU-xylVbOXXaP9QbDvbeGLWeyKR7IqnrMo6lxk0hDP4m2PcM28vpgfCfLEyh1yzOy_4LVEN87JPUFtOyFA_5QMVWjcPSP4bSVfKMOM9YXjfqiWYSX8jvfYdjLrlzdKp8Y8Zsh9SazkJlb-U5tbE_L3stVpxY9eucMbWG2dGjatgE8dVJSiEfuBRo_zAZFBaOg4HiICYYH5bUNtA4MVpavgwS_MqKzdbCWN_g_Uhl10UgaJEZXfBRUpT0CV4JYOUFySfId3i2JDJLr4M8gGceny5PlZLpczueL-TIMo3gUNEEyPZ2chPE8jpfRIgpnYXQzClqlEBGeTBeniyiehDN8vljiqWd5H7uHfVDOhFH6rD_Yu_P95n_O2aRQ)


[dcr]: ./DCR.png
[AD5933]: https://www.analog.com/en/products/ad5933.html#product-overview
[inductance]: ./Inductance.png